# Blockchain Java Library

### Author: Viktor Zagrebin

### Supported versions:
Java 17, Gradle 8.8, JUnit5

### Generate library, docs and sources jars

./gradlew clean build

### Generate java docs

./gradlew javadoc

### Verify library

jar tf lib/build/libs/lib.jar

### Import library
Use the library in other Java projects by adding a dependency on your
library’s JAR file to the project’s build.gradle file, for example:

dependencies {  implementation files('libs/bc-1.0-SNAPSHOT.jar') }

