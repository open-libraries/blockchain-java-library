package com.linstam.bc;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Block is a part of blockchain network
 */
public class Block {

  private LocalDateTime blockTime;
  private List<Transaction> transactions;

  /**
   * Create the block
   * @param blockTime block time
   * @param transactions transactions
   */
  public Block(LocalDateTime blockTime, List<Transaction> transactions) {
    if (blockTime == null) {
      throw new IllegalArgumentException("Block time cannot be null");
    }
    this.blockTime = blockTime;
    this.transactions = transactions;
  }

  /**
   * Get creation time of this block
   *
   * @return Creation time of this block
   */
  public LocalDateTime getBlockTime() {
    return blockTime;
  }

  /**
   * Set creation time of this block
   *
   * @param blockTime block time creation
   */
  public void setBlockTime(LocalDateTime blockTime) {
    this.blockTime = blockTime;
  }

  /**
   * Get transactions included in this block
   *
   * @return Transactions assigned with this block
   */
  public List<Transaction> getTransactions() {
    return transactions;
  }

  /**
   * Include transactions into this block
   *
   * @param transactions transactions
   */
  public void setTransactions(List<Transaction> transactions) {
    this.transactions = transactions;
  }
}

