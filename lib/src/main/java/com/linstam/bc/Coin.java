package com.linstam.bc;

import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Predicate;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingLong;

/**
 * The coin which represents a coin structure in the blockchain network
 *
 * @author <a href="mailto:zagrebin.victor@gmail.com">Zagrebin Viktor</a>
 */
public record Coin(Transaction creatorTransaction,
                   Transaction spenderTransaction,
                   String address,
                   long amount) {

  /**
   * Creates the coin structure with creator and spender transactions, amount and address
   *
   * @param creatorTransaction the list of transactions which create the coins
   * @param spenderTransaction the list of transactions which spend the coins
   * @param address address the address to which the coins are associated
   * @param amount coins amount
   */
  public Coin {
    if (address == null) {
      throw new IllegalArgumentException("address cannot be null");
    }
    if (amount == 0L) {
      throw new IllegalArgumentException("amount cannot be 0");
    }
    if(creatorTransaction == null && spenderTransaction == null) {
      throw new IllegalArgumentException("creatorTransaction and spenderTransaction cannot be both null");
    }
  }

  /**
   * Coin A is called the parent of coin B if B is one of the outputs of transaction T, and A is one of the inputs
   *
   * @param b a coin in relation to which this coin may be the parent
   * @return Returns true if this coin is a parent of coin B. False otherwise.
   */
  public boolean isParentOf(Coin b) {
    if(b == null) {
      throw new IllegalArgumentException("The coin parameter can't be null");
    }
    Transaction transaction = this.spenderTransaction;  //extract transaction where this coin is a spender
    return transaction.isSpender(this)  // this coin is one of the inputs
             && transaction.isCreator(b);    // B is one of the outputs
  }

  /**
   * Coin A is called an ancestor of coin B if there exists a chain of coins C0,C1 ,...,Cn
   * such that A is a parent of C0, Cn is a parent of B, and Ci is the parent of Ci+1 foreach i.
   * If A is the parent of B then A is also an ancestor of B.
   *
   * @param b a coin in relation to which A may be the ancestor
   * @return Returns true if this coin is an ancestor of coin B. False otherwise.
   */
  public boolean isAncestorOf(Coin b) {
    if(b == null) {
      throw new IllegalArgumentException("The coin parameter can't be null");
    }
    if(this.spenderTransaction == null) return false; //coin A was not spent so there is no ancestors exist

    boolean isAncestor;
    ArrayList<Coin> coinChain = new ArrayList<>(); //to access by index of the last traversed coin
    coinChain.add(this); //add coin A as a start point
    int index = 0; //index to remember the last traversed coin

    do {
      Coin coin = coinChain.get(index);
      //the coin b might be spent or not spent
      //if b is not among child coins(spent or not spent)
      //then the farther search should process child spent coins only

      var childSpentCoins = getChildSpentCoins(coin);
      var childNonSpentCoins = getChildNonSpentCoins(coin);

      isAncestor = childNonSpentCoins.stream().anyMatch(c -> c.equals(b))
                     || childSpentCoins.stream().anyMatch(c -> c.equals(b));

      //stop if b is found or if no child spent coins found
      if(isAncestor || (!isAncestor && childSpentCoins.size() == 0)) break;
      if (!isAncestor && childSpentCoins.size() != 0) {
        coinChain.addAll(++index, childSpentCoins);
        coin = coinChain.get(index);  //extract next coin to process
        if(coin.spenderTransaction == null) {
          index++; //increase the index of the coin to process
        }
      }
    } while(!isAncestor);

    return isAncestor;
  }


  /**
   *
   * Get all Coinbase ancestors for this coin.
   *
   * Algorithm steps:
   * <p>
   * 1. From the creatorTransaction of the coin we get a non-empty list of inputs
   *    and save the found ancestors to the collection for father traversal.
   * 2. If all elements in coinsChain have been traversed,
   *    no inputs were found, and the transaction is not Coinbase (a Coinbase transaction may have an empty list of inputs),
   *    then we end the search for the coin and return null. Returning null means no coinbase ancestors were found.
   *    The isCoinbase=true flag can only be used in final transactions,
   *    so the absence of a flag at a particular level should not stop the search.
   * 3. If the current transaction is Coinbase, then we save also its inputs.
   *    Use of the custom type is allowed under the condition.
   * </p>
   *
   * @return All Coinbase ancestors for this coin.
   * @see CoinbaseAncestors
   */
  public CoinbaseAncestors findAllCoinbaseAncestors() {
    var coinsChain = new ArrayList<Coin>();
    var childToAncestorsMap = new LinkedHashMap<Coin, List<Coin>>();
    coinsChain.add(this);
    int index = 0;
    boolean isCoinbase;
    boolean isNoMoreCoinsToTraverse;

    do {
      Coin currentCoin = coinsChain.get(index);
      var transaction = currentCoin.creatorTransaction;
      isCoinbase = transaction.isCoinbase();
      var inputs = transaction.getInputs();

      if(inputs != null && !inputs.isEmpty()) {
        //Do not add inputs for Coinbase transaction to prevent farther search
        if(!isCoinbase) {
          coinsChain.addAll(++index, inputs);
        }
      }
      if(isCoinbase) {
        //Add coinbase ancestors(inputs might be null) only and nearest child
        childToAncestorsMap.putIfAbsent(currentCoin, inputs);
      }
      if(inputs == null && isCoinbase) index++; //consider Coinbase coin traversed
      isNoMoreCoinsToTraverse = index == coinsChain.size();
      // All elements in coinsChain have been traversed.
      // There is no inputs for this transaction. It's not a Coinbase. Stop search. Return null.
      if((inputs == null || inputs.isEmpty()) && !isCoinbase  && isNoMoreCoinsToTraverse) return null;
    } while(!isNoMoreCoinsToTraverse);

    return new CoinbaseAncestors(childToAncestorsMap);
  }

  /**
   * Finds the address that received the maximum total value of coins for a given time range.
   *
   * Algorithm steps:
   * <p>
   * 1. Define blocks that fall within a given time interval.
   *    Time range can only be obtained from blocks.
   *    The time interval suggests that there are several blocks in which blockTime is in this interval.
   * 2. Find the related transactions. This list of transactions should fall into blocks that
   *    fall within the specified time interval.
   *    I’m guessing the time of the block, represents the time of transactions.
   * 3. Find the addresses where the coins related to the transactions found were sent to.
   * 4. Select the address with the maximum amount of coins received.
   *   The null return indicates that the address was not found.
   *   For example, nobody who received the coins was in the range.
   * </p>
   *
   * @param blockchain the blockchain with blocks
   * @param intervalStart the starting date of the time interval
   * @param intervalEnd the ending date of the time interval
   * @param areSpentCoins the flag to identify the type of included coins.
   *    True if choose spent coins only,
   *    false if choose non spent coins only,
   *    null if choose both spent and not spent coins.
   * @return The address that received the maximum total value of coins for a given time range.
   */
  public String findMaximumInboundVolumeAddress(final Blockchain blockchain,
                                                final LocalDateTime intervalStart,
                                                final LocalDateTime intervalEnd,
                                                final Boolean areSpentCoins) {
    if(blockchain == null) {
      throw new IllegalArgumentException("blockchain can't be null");
    }
    if(intervalStart == null || intervalEnd == null) {
      throw new IllegalArgumentException("intervalStart or intervalEnd can't be null");
    }
    if(!intervalEnd.isAfter(intervalStart)) {
      throw new IllegalArgumentException("intervalEnd should be after intervalStart");
    }

    final Predicate<Coin> isNotNull = Objects::nonNull;
    final Predicate<Coin> nonSpentCoins = coin -> coin.spenderTransaction == null;
    final Predicate<Coin> spentCoins = coin -> coin.spenderTransaction != null;
    final Predicate<Coin> byConditon;
    if(areSpentCoins == null) {
      byConditon = isNotNull;
    } else if(areSpentCoins) {
      byConditon = isNotNull.and(spentCoins);
    } else {
      byConditon = isNotNull.and(nonSpentCoins);
    }

    //get coins (spent/son spent/all depending on byCondition filter's predicate) for a given time range
    var coins = blockchain.getBlocksWithinTimeInterval(intervalStart, intervalEnd).parallelStream()
        .flatMap(b -> b.getTransactions().stream())
        .filter(t -> t != null && t.getInputs() != null)
        .flatMap(t -> t.getInputs().stream())
        .filter(byConditon)
        .toList();

    var address = coins.parallelStream()
        //LinkedHashMap to keep operation idempotent
        .collect(groupingBy(coin -> coin.address, LinkedHashMap::new, summingLong(coin -> coin.amount)))
        .entrySet()
        .stream()
        .max(Map.Entry.comparingByValue())
        .map(e -> e.getKey()) // entry with max key if available
        .orElse(null);  // null if not available

    return address;
  }


  /**
   * Nearest descendant coins that have been spent
   *
   * @param coin the coin for which we are looking for the nearest descendant coins
   * @return Nearest descendant coins that have been spent
   */
  private List<Coin> getChildSpentCoins(Coin coin) {
    return coin.spenderTransaction.getOutputs().stream()
        .filter(c -> c.spenderTransaction != null)
        .toList();
  }


  //

  /**
   * Nearest descendant coins that have not been spent
   *
   * @param coin the coin for which we are looking for the nearest descendant coins
   * @return Nearest descendant coins that have not been spent
   */
  private List<Coin> getChildNonSpentCoins(Coin coin) {
    return coin.spenderTransaction.getOutputs().stream()
        .filter(c -> c.spenderTransaction == null)
        .toList();
  }
}
