package com.linstam.bc;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Blockchain network
 */
public class Blockchain {

  /**
   * The collection of Blocks is ordered by block_time in ascending order,
   * i.e. if i > j then blocks[i].block_time >= blocks[j].block_time
   */
  private TreeSet<Block> blocks;
  private final Comparator<Block> BLOCK_COMPARATOR = Comparator.comparing(Block::getBlockTime);

  /**
   * Creates blockchain network with an empty collection of the blocks
   */
  public Blockchain() {
    this.blocks = new TreeSet<>(BLOCK_COMPARATOR);
  }

  /**
   * Create blockchain network and include collection of blocks into it
   *
   * @param blocks the blocks to include into the blockchain network
   */
  public Blockchain(Collection<Block> blocks) {
    this();
    if (blocks == null) {
      throw new IllegalArgumentException("blocks cannot be null");
    }
    this.blocks = blocks.stream()
                        .collect(Collectors.toCollection(() -> new TreeSet<>(BLOCK_COMPARATOR)));
  }

  /**
   * Returns all blocks from the blockchain network
   *
   * @return All blocks from the blockchain network
   */
  public TreeSet<Block> getBlocks() {
    return blocks;
  }

  /**
   * Returns all blocks whose creation time is within a defined time interval
   *
   * @param intervalStart the start date of the time interval
   * @param intervalEnd the end date of the time interval
   * @return The blocks whose creation time is within a defined time interval
   */

  public List<Block> getBlocksWithinTimeInterval(LocalDateTime intervalStart, LocalDateTime intervalEnd) {
    return this.getBlocks().stream()
        .filter(b -> b.getBlockTime().isAfter(intervalStart) && b.getBlockTime().isBefore(intervalEnd))
        .toList();
  }

  void add(Block block) {
     this.getBlocks().add(block);
  }

  void clear() {
    this.getBlocks().clear();
  }
}
