package com.linstam.bc;

import java.util.List;

/**
 * The structure which represents transaction in the blockchain network
 */
public class Transaction {

  private Block block;
  private boolean isCoinbase;
  private List<Coin> inputs;
  private List<Coin> outputs;

  /**
   * Creates transaction for the blockchain network
   *
   * @param block the block the transaction is associated with
   * @param isCoinbase the flag indicating the Coinbase origin
   */
  public Transaction(Block block, boolean isCoinbase) {
    if (block == null) {
      throw new IllegalArgumentException("block cannot be null");
    }
    this.isCoinbase = isCoinbase;
    this.block = block;
  }

  /**
   * Get a block this transaction is associated with
   *
   * @return a block this transaction is associated with
   */
  public Block getBlock() {
    return block;
  }

  /**
   * Set the block this transaction is associated with
   *
   * @param block the block this transaction is associated with
   */
  public void setBlock(Block block) {
    this.block = block;
  }

  /**
   * Get a flag indicating the Coinbase origin
   *
   * @return The flag indicating the Coinbase origin. True if it has the Coinbase origin.
   */
  public boolean isCoinbase() {
    return isCoinbase;
  }

  /**
   * Set the flag indicating the Coinbase origin
   *
   * @param coinbase the flag indicating the Coinbase origin
   */
  public void setCoinbase(boolean coinbase) {
    isCoinbase = coinbase;
  }

  /**
   * Check if this transaction has a spender type for the defined coin
   *
   * @param coin the coin
   * @return True if this transaction has a spender type for the defined coin
   */
  public boolean isCreator(Coin coin) {
    return outputs.contains(coin);
  }

  /**
   * Check if this transaction has a creator type for the defined coin
   *
   * @param coin the coin
   * @return True if this transaction has a creator type for the defined coin
   */
  public boolean isSpender(Coin coin) {
    return inputs.contains(coin);
  }

  /**
   * Get the list of inputs for this transaction
   *
   * @return The list of inputs for this transaction
   */
  public List<Coin> getInputs() {
    return inputs;
  }

  /**
   * Set the inputs for this transaction
   *
   * @param inputs the list of coins as inputs
   */
  public void setInputs(List<Coin> inputs) {
    this.inputs = inputs;
  }

  /**
   * Set the outputs for this transaction
   *
   * @param outputs the list of coins as inputs
   */
  public void setOutputs(List<Coin> outputs) {
    this.outputs = outputs;
  }

  /**
   * Get the list of outputs for this transaction
   *
   * @return The list of outputs for this transaction
   */
  public List<Coin> getOutputs() {
    return outputs;
  }
}
