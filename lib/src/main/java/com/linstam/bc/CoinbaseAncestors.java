package com.linstam.bc;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * The coinbase ancestor is a structure to keep coinbase ancestors with their lists of inputs (coins)
 * and keeps the adding order of coins
 *
 * @author <a href="mailto:zagrebin.victor@gmail.com">Zagrebin Viktor</a>
 */
public record CoinbaseAncestors(LinkedHashMap<Coin, List<Coin>> ancestorToInputsMap) {}
