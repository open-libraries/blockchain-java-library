package com.linstam.bc;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class AddressTest {
    private static final LocalDateTime INTERVAL_START = LocalDateTime.of(2023,01,01,10,20);
    private static final LocalDateTime INTERVAL_END = LocalDateTime.of(2024,02,02,10,20);
    private static final String ADDRESS_WITH_MAX_TOTAL_VALUE = "1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf6";
    private static Blockchain blockchain;
    private static Coin a1;

    @BeforeAll
    /**
     * A1(t1,t2) -> B1(t3,t4), B2(t5,t6), B3(t7,t8), B4(t9,t10)
     *  B4(t9,t10) -> D4(t15, null), D5(t16, t17), D6(t18,null)
     *  B3(t7,t8) -> D1(t11,null), D2(t12,null), D3(t13,t14)
     *  B2(t5,t6) -> E4(t23,null)
     *  B1(t3,t4) -> E1(t19),E2(t20),E3(t21)
     *
     *  D5,D6 within time interval, one address, max
     *  D1,D2,D3 within time interval, one address, max value
     *  E1,E2,E3 within time interval, one address, not max value
     *
     *  D5, D3 spent coins
     *  D6, D1, D2 not spent coins
     *
     *  D4, E4 not within time interval
     */
    static void setup() {
        final Block block1 = new Block(LocalDateTime.of(2021,05,21,10,20),null);
        final Block block2 = new Block(LocalDateTime.of(2022,05,21,10,20),null);

        // block3, block4, block5 will fit into the specified time interval
        final Block block3 = new Block(LocalDateTime.of(2023,05,21,10,20),null);
        final Block block4 = new Block(LocalDateTime.of(2023,07,21,10,20),null);
        final Block block5 = new Block(LocalDateTime.of(2024,01,01,10,20),null);

        final Block block6 = new Block(LocalDateTime.of(2024,03,01,10,20),null);

        final Transaction t1 = new Transaction(block1,false);
        final Transaction t2 = new Transaction(block1,false);
        final Transaction t3 = new Transaction(block2, false);
        final Transaction t4 = new Transaction(block2, false);
        final Transaction t5 = new Transaction(block2, false);
        final Transaction t6 = new Transaction(block2, false);
        final Transaction t7 = new Transaction(block2, false);
        final Transaction t8 = new Transaction(block2, false);
        final Transaction t9 = new Transaction(block2, false);
        final Transaction t10 = new Transaction(block2, false);

        // all transactions from blocks 3-5 will fit into the specified time interval
        final Transaction t11 = new Transaction(block3, false);
        final Transaction t12 = new Transaction(block3, false);
        final Transaction t13 = new Transaction(block3, false);
        final Transaction t14 = new Transaction(block3, false);
        final Transaction t16 = new Transaction(block5, false);
        final Transaction t17 = new Transaction(block5, false);
        final Transaction t18 = new Transaction(block5, false);
        final Transaction t19 = new Transaction(block4, false);
        final Transaction t20 = new Transaction(block4, false);
        final Transaction t21 = new Transaction(block4, false);
        final Transaction t22 = new Transaction(block4, false);

        final Transaction t15 = new Transaction(block6, false);
        final Transaction t23 = new Transaction(block6, false);

        a1 = new Coin(t1,t2,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf1",108);

        final Coin b1 = new Coin(t3,t4,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf2",18);
        final Coin b2 = new Coin(t5,t6,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf3",3);
        final Coin b3 = new Coin(t7,t8,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf4",42);
        final Coin b4 = new Coin(t9, t10,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf5",45);

        //d1,d2,d3 are assigned with one address
        //d5,d6 are assigned with another address
        final Coin d1 = new Coin(t11,null,ADDRESS_WITH_MAX_TOTAL_VALUE,20);
        final Coin d2 = new Coin(t12,null,ADDRESS_WITH_MAX_TOTAL_VALUE,10);
        final Coin d3 = new Coin(t13,t14,ADDRESS_WITH_MAX_TOTAL_VALUE,12);
        final Coin d4 = new Coin(t15,null,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf7",3);
        final Coin d5 = new Coin(t16,t17,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf8 ",12);
        final Coin d6 = new Coin(t18,null,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf8",30);

        //e1,e2,e3 are assigned with one address
        final Coin e1 = new Coin(t7,null,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf9",3);
        final Coin e2 = new Coin(t7,null,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf9",6);
        final Coin e3 = new Coin(t7,null,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf9",9);
        final Coin e4 = new Coin(t7,null,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf10",3);

        t1.setInputs(List.of());              //t1 is a creator transaction of coin A1
        t3.setInputs(List.of(b1));            //t3 is a creator transaction of coin B1
        t5.setInputs(List.of(b2));            //t5 is a creator transaction of coin B2
        t7.setInputs(List.of(b3));            //t7 is a creator transaction of coin B3
        t9.setInputs(List.of(b4));            //t9 is a creator transaction of coin B4

        t2.setInputs(List.of(a1));            //t2 is a spender transaction of coin A1
        t2.setOutputs(List.of(b1,b2,b3,b4));  //B1,B2,B3,B4 are the child coins for coin A1
        t4.setInputs(List.of(b1));            //t4 is a spender transaction of coin B1
        t4.setOutputs(List.of(e1,e2,e3));     //E1,E2,E3 are the child coins for coin B1
        t6.setInputs(List.of(b2));            //t6 is a spender transaction of coin B2
        t6.setOutputs(List.of(e4));           //E4 coin is the child coin for coin B2
        t8.setInputs(List.of(b3));            //t8 is a spender transaction of coin B3
        t8.setOutputs(List.of(d1,d2,d3));     //D1,D2,D3 are the child coins for coin B3
        t10.setInputs(List.of(b4));           //t10 is a spender transaction of coin B4
        t10.setOutputs(List.of(d4,d5,d6));    //D4,D5,D6 are the child coins for coin B4

        t11.setInputs(List.of(d1));           //t11 is a creator transaction of coin D1
        t12.setInputs(List.of(d2));           //t12 is a creator transaction of coin D2
        t13.setInputs(List.of(d3));           //t13 is a creator transaction of coin D3
        t14.setOutputs(List.of());            //D3 is spent

        t15.setInputs(List.of(d4));           //t15 is a creator transaction of coin d4
        t16.setInputs(List.of(d5));           //t16 is a creator transaction of coin d5
        t18.setInputs(List.of(d6));           //t18 is a creator transaction of coin d6
        t17.setOutputs(List.of());            //D5 is spent

        t19.setInputs(List.of(e1));           //t19 is a creator transaction of coin E1
        t20.setInputs(List.of(e2));           //t20 is a creator transaction of coin E2
        t21.setInputs(List.of(e3));           //t21 is a creator transaction of coin E3
        t23.setInputs(List.of(e4));           //t23 is a creator transaction of coin E4
        t22.setOutputs(List.of());            //E3 is spent

        block1.setTransactions(List.of(t1,t2));
        block2.setTransactions(List.of(t3,t4,t5,t6,t7,t8,t9,t10));

        block3.setTransactions(List.of(t11,t12,t13,t14));
        block4.setTransactions(List.of(t19,t20,t21,t22));
        block5.setTransactions(List.of(t16,t17,t18));

        block6.setTransactions(List.of(t15,t23));

        blockchain = new Blockchain(List.of(block1,block2,block3,block4,block5,block6));
    }

    @Test
    @DisplayName("Find address that received the maximum total value of coins for a given time range")
    void checkAddressWithMaxValueForGivenTimeRange() {
        final String addressToCompare = "1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf6";
        final String address = a1.findMaximumInboundVolumeAddress(blockchain, INTERVAL_START, INTERVAL_END, null);
        assertEquals(addressToCompare, address,
            "It should be an address that received the maximum total value of coins for a given time range");
    }

    @Test
    @DisplayName("Check idempotentcy when two addresses received the maximum total value of non spent coins for a given time range")
    void checkAddressWithMaxValueOfNonSpentCoinsForGivenTimeRangeWhenTwoAddressesHaveMaxValue() {
        //1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf6 and 1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf8 keep the same value
        //should always return 1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf6
        final String addressToCompare = "1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf6";
        final String address1 = a1.findMaximumInboundVolumeAddress(blockchain, INTERVAL_START, INTERVAL_END, false);
        assertEquals(addressToCompare, address1,
            "It should be always the same address among the addresses with the same maximum total value of coins for a given time range");
        final String address2 = a1.findMaximumInboundVolumeAddress(blockchain, INTERVAL_START, INTERVAL_END, false);
         assertEquals(addressToCompare, address2,
             "It should be always the same address among the addresses with the same maximum total value of coins for a given time range");
    }

    @Test
    @DisplayName("Check idempotentcy when two addresses received the maximum total value of spent coins for a given time range")
    void checkAddressWithMaxValueOfSpentCoinsForGivenTimeRangeWhenTwoAddressesHaveMaxValue() {
        //1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf6 and 1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf8 keep the same value
        //should always return 1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf6
        final String addressToCompare = "1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf6";
        final String address1 = a1.findMaximumInboundVolumeAddress(blockchain, INTERVAL_START, INTERVAL_END, true);
        assertEquals(addressToCompare, address1,
            "It should be always the same address among the addresses with the same maximum total value of spent coins for a given time range");
        final String address2 = a1.findMaximumInboundVolumeAddress(blockchain, INTERVAL_START, INTERVAL_END, true);
        assertEquals(addressToCompare, address2,
            "It should be always the same address among the addresses with the same maximum total value of spent coins for a given time range");
    }

    @Test
    @DisplayName("Find address that received the maximum total value of coins for a non applicable time range")
    void checkNonApplicablaTimeInterval() {
        final LocalDateTime invalidIntervalStart = LocalDateTime.of(2024,9,01,10,20);
        final LocalDateTime invalidIntervalEnd = LocalDateTime.of(2024,11,02,10,20);
        final String address = a1.findMaximumInboundVolumeAddress(blockchain, invalidIntervalStart, invalidIntervalEnd, true);
        assertNull(address, "The address should be null for non applicable time interval");
    }
}
