package com.linstam.bc;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BlockTest {

    @Test
    @DisplayName("The blocks list is ordered by block_time in ascending order")
    void checkWhenBlocksOrderedByTimeASC() {
        final Block block1 = new Block(LocalDateTime.of(2023,05,21,10,20),null);
        final Block block2 = new Block(LocalDateTime.of(2022,05,21,10,20),null);
        final Block block3 = new Block(LocalDateTime.of(2021,05,21,10,20),null);

        final Blockchain blockchain = new Blockchain(List.of(block1,block2,block3));

        final List<Block> blocks = new ArrayList<>(blockchain.getBlocks());
        boolean isOrdered = blocks.get(0).getBlockTime().isBefore(blocks.get(1).getBlockTime());
        isOrdered = isOrdered && blocks.get(1).getBlockTime().isBefore(blocks.get(2).getBlockTime());

        assertTrue(isOrdered, "The blocks list should be ordered by block_time in ascending order");
    }


}
