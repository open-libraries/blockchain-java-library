package com.linstam.bc;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ParentTest {

    @Test
    @DisplayName("Coin A is a parent for coin B")
    /**
     * Coin A is called the parent of coin B if B is one of the outputs of transaction T, and A is one of the inputs.
     * A(t1,t2) -> C11(t3,null), B(t4,null)
     */
    void checkWhenACoinIsAParentOfBCoin() {
        final Block block1 = new Block(LocalDateTime.of(2021,05,21,10,20),null);

        final Transaction t1 = new Transaction(block1,false);
        final Transaction t2 = new Transaction(block1,false);
        final Transaction t3 = new Transaction(block1, false);
        final Transaction t4 = new Transaction(block1, false);
        final List<Transaction> transactions = List.of(t1,t2,t3,t4);

        block1.setTransactions(transactions);

        final Blockchain blockchain = new Blockchain(List.of(block1));

        final Coin a = new Coin(t1,t2,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf1",5);
        final Coin c11 = new Coin(t3,null,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf1",2);
        final Coin b = new Coin(t4,null,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf1",3);

        t2.setInputs(List.of(a));
        t2.setOutputs(List.of(b,c11)); //t2 is a spender transaction of coin A
                                       //coin B and coin C11 are the child coins for coin A
        t3.setInputs(List.of(a));      //t3 is a creator transaction of coin C11
        t4.setInputs(List.of(a));      //t4 is a creator transaction of coin B

        assertTrue(a.isParentOf(b), "Coin A should be a parent for coin B");
    }

    @Test
    @DisplayName("Coin A is Not a parent for coin B")
    /**
     * A(t1,t2) -> C11(t3,null), C12(t4,t5) -> B(t6,null)
     */
    void checkWhenACoinIsNotAParentOfBCoin() {
        final Block block1 = new Block(LocalDateTime.of(2021,05,21,10,20),null);

        final Transaction t1 = new Transaction(block1,false);
        final Transaction t2 = new Transaction(block1,false);
        final Transaction t3 = new Transaction(block1, false);
        final Transaction t4 = new Transaction(block1, false);
        final Transaction t5 = new Transaction(block1, false);
        final Transaction t6 = new Transaction(block1, false);
        final List<Transaction> transactions = List.of(t1,t2,t3,t4,t5,t6);

        block1.setTransactions(transactions);

        final Blockchain blockchain = new Blockchain(List.of(block1));

        final Coin a = new Coin(t1,t2,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf1",5);
        final Coin c11 = new Coin(t3,null,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf1",2);
        final Coin c12 = new Coin(t4,t5,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf1",3);
        final Coin b = new Coin(t6,null,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf1",3);

        t2.setInputs(List.of(a));
        t2.setOutputs(List.of(c11));   //t2 is a spender transaction of coin A
                                       //coin C11 is the child coin for coin A
        t3.setInputs(List.of(a));      //t3 is a creator transaction of coin C11
        t5.setOutputs(List.of(c12));   //t5 is a spender transaction of coin C12
                                       //coin B is the child coin for coin C12
        t6.setInputs(List.of(c12));    //t6 is a creator transaction of coin B

        assertFalse(a.isParentOf(b), "Coin A should Not be a parent for coin B");
    }
}
