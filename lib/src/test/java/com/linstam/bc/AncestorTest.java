package com.linstam.bc;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;

import static java.lang.System.out;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AncestorTest {

    @Test
    @DisplayName("Coin A is an ancestor for coin B")
    /**
     * A(t1,t2) -> C1(t3,null), C2(t4,null), C3(t5,t6) -> B(t7,null)
     */
    void checkWhenACoinIsAnAncestorOfBCoin() {
        final Block block1 = new Block(LocalDateTime.of(2021,05,21,10,20),null);
        final Block block2 = new Block(LocalDateTime.of(2022,05,21,10,20),null);
        final Block block3 = new Block(LocalDateTime.of(2023,05,21,10,20),null);

        final Transaction t1 = new Transaction(block1,false);
        final Transaction t2 = new Transaction(block1,false);
        final Transaction t3 = new Transaction(block2, false);
        final Transaction t4 = new Transaction(block2, false);
        final Transaction t5 = new Transaction(block2, false);
        final Transaction t6 = new Transaction(block3, false);
        final Transaction t7 = new Transaction(block3, false);

        final Coin a = new Coin(t1,t2,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf1",10);
        final Coin c11 = new Coin(t3,null,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf2",2);
        final Coin c12 = new Coin(t4,null,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf3",3);
        final Coin c13 = new Coin(t5,t6,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf4",5);
        final Coin b = new Coin(t7,null,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf5 ",5);

        t1.setInputs(List.of());             //t1 is a creator transaction of coin A
        t2.setInputs(List.of(a));            //t2 is a spender transaction of coin A
        t2.setOutputs(List.of(c11,c12,c13)); //C11,C12,C13 coins are the child coins for coin A

        t3.setInputs(List.of(c11));   //t3 is a creator transaction of coin C11
        t4.setInputs(List.of(c12));   //t4 is a creator transaction of coin C12
        t5.setInputs(List.of(c13));   //t5 is a creator transaction of coin C13

        t6.setInputs(List.of(c13));   //t6 is a spender transaction of coin C13
        t6.setOutputs(List.of(b));    //Coin B is child of coin C13

        t7.setInputs(List.of(b));     //t7 is a creator transaction of coin B

        block1.setTransactions(List.of(t1,t2));
        block2.setTransactions(List.of(t3,t4,t5));
        block2.setTransactions(List.of(t6,t7));

        final Blockchain blockchain = new Blockchain(List.of(block1,block2,block3));

        assertTrue(a.isAncestorOf(b), "Coin A should be an ancestor for coin B");
    }

    @Test
    @DisplayName("Coin A is Not an ancestor for coin B")
    /**
     *  A(t1,t2) -> C1(t3,null), C2(t4,null); D(t5,t6) -> B(t7,null)
     */
    void checkWhenACoinIsNotAnAncestorOfBCoin() {
        final Block block1 = new Block(LocalDateTime.of(2021,05,21,10,20),null);
        final Block block2 = new Block(LocalDateTime.of(2022,05,21,10,20),null);
        final Block block3 = new Block(LocalDateTime.of(2023,05,21,10,20),null);

        final Transaction t1 = new Transaction(block1,false);
        final Transaction t2 = new Transaction(block1,false);
        final Transaction t3 = new Transaction(block2, false);
        final Transaction t4 = new Transaction(block2, false);
        final Transaction t5 = new Transaction(block2, false);
        final Transaction t6 = new Transaction(block3, false);
        final Transaction t7 = new Transaction(block3, false);

        final Coin a = new Coin(t1,t2,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf1",10);
        final Coin c11 = new Coin(t3,null,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf2",2);
        final Coin c12 = new Coin(t4,null,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf3",3);
        final Coin d = new Coin(t5,t6,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf4",5);
        final Coin b = new Coin(t7,null,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf5 ",5);

        t1.setInputs(List.of());           //t1 is a creator transaction of coin A
        t2.setInputs(List.of(a));          //t2 is a spender transaction of coin A
        t2.setOutputs(List.of(c11,c12));   //C11,C12 coins are the child coins for coin A

        t3.setInputs(List.of(c11));   //t3 is a creator transaction of coin C11
        t4.setInputs(List.of(c12));   //t4 is a creator transaction of coin C12

        t5.setInputs(List.of());
        t6.setInputs(List.of(d));   //t6 is a spender transaction of coin D
        t6.setOutputs(List.of(b));  //Coin B is child of coin D

        t7.setInputs(List.of(b));   //t7 is a creator transaction of coin B

        block1.setTransactions(List.of(t1,t2));
        block2.setTransactions(List.of(t3,t4,t5));
        block3.setTransactions(List.of(t6,t7));

        final Blockchain blockchain = new Blockchain(List.of(block1,block2,block3));

        assertFalse(a.isAncestorOf(b), "Coin A should Not be an ancestor for coin B");
    }

    @Test
    @DisplayName("Check all Coinbase ancestors")
    /**
     *  C0(t0,t2)                           -> C4(t2,null)
     *  C1(t0,t1) -> C3(t1,null), C2(t1,t2) -^
     *
     *  Only C0 and C1 are Coinbase ancestors
     */
    void checkAllCoinbaseAncestors() {
        final Block block1 = new Block(LocalDateTime.of(2021,05,21,10,20),null);

        final Transaction t0 = new Transaction(block1,false);
        final Transaction t1 = new Transaction(block1,false);
        final Transaction t2 = new Transaction(block1, false);

        final List<Transaction> transactions = List.of(t0,t1,t2);

        block1.setTransactions(transactions);

        final Blockchain blockchain = new Blockchain(List.of(block1));

        final Coin c0 = new Coin(t0,t2,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf1",5);
        final Coin c1 = new Coin(t0,t1,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf2",10);
        final Coin c2 = new Coin(t1,t2,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf3",3);
        final Coin c3 = new Coin(t1,null,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf4",7);
        final Coin c4 = new Coin(t2,null,"1AG24pctoCpEMfSvEKfUZqaGYnz8ey8Bf5",8);

        t0.setCoinbase(true);             //t0 is a Coinbase transaction
        t0.setOutputs(List.of(c0,c1));    //t0 is a creator transaction of coin C0, C1

        t1.setOutputs(List.of(c2,c3));    //t1 is a creator transaction of coin C2, C3
        t1.setInputs(List.of(c1));        //C1 is an input for t1 transaction
        t2.setInputs(List.of(c2,c0));     //C2 and C0 are the inputs for t2 transaction
        t2.setOutputs(List.of(c4));       //t2 is a creator transaction of coin C4

        var ancestors = c4.findAllCoinbaseAncestors().ancestorToInputsMap();
        boolean areAllCoinbaseAncestors = ancestors.size() == 2
            && ancestors.containsKey(c0)
            && ancestors.containsKey(c1);

        assertTrue(areAllCoinbaseAncestors, "All ancestors should be Coinbase");
    }
}
